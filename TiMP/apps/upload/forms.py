from django import forms
from TiMP.apps.upload import models

Overwrite_choice = [
    ('yes', 'Да'),
    ('no', 'Нет'),
]


class Yandex_upload(forms.Form):
    path = forms.CharField(max_length=100, label="Путь загрузки", initial="", required=False,
                           help_text="Пример: example/deep2/deep3")
    overwrite_choice = forms.ChoiceField(choices=Overwrite_choice, widget=forms.RadioSelect(),
                                         label="Перезаписать данные", initial="no", required=False)


class Yandex_download(forms.Form):
    path_file = forms.CharField(max_length=100, label="Путь до файла для скачивания", initial="",
                                help_text="Пример: example/some-file-to-download.txt")


class Yandex_mkdir(forms.Form):
    path_dir = forms.CharField(max_length=100, label="Конечное расположение папки", initial="",
                               help_text="Пример: example/what_i_want")


class Yandex_remove(forms.Form):
    path_remove = forms.CharField(max_length=100, label="Путь до удаляемого объекта", initial="",
                                  help_text="Пример: example ; file.txt ; example/file/file.txt")


class web_screenshot(forms.Form):
    url = forms.URLField(initial='http://', label="Адрес сайта для скриншота",
                         help_text="http://docs.djangoproject.com")


class diary(forms.ModelForm):
    class Meta:
        model = models.DiaryModel
        fields = ['diary']
        labels = {
            'diary': ''
        }
        widgets = {
            'diary': forms.Textarea(attrs={'cols': 80, 'rows': 25})
        }


class notes(forms.ModelForm):
    class Meta:
        model = models.NotesModel
        fields = ['notes']
        labels = {
            'notes': ''
        }
        widgets = {
            'notes': forms.Textarea(attrs={'cols': 40, 'rows': 10})
        }


class notes_delete(forms.Form):
    notes = forms.ChoiceField(choices=[], label="Удалить заметку")

    def __init__(self, *args, **kwargs):
        user_id = kwargs.pop('user_id', None)
        notes_choice = []
        for i in models.NotesModel.objects.filter(user_id=user_id):
            notes_choice.append((i.id, i.id))
        super(notes_delete, self).__init__(*args, **kwargs)
        self.fields['notes'].choices = notes_choice


class calendar(forms.Form):
    date = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#datetimepicker1'
        })
    )