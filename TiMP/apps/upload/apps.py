from django.apps import AppConfig


class UploadConfig(AppConfig):
    name = 'TiMP.apps.upload'
