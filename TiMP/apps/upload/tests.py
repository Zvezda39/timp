from django.test import TestCase

# Create your tests here.

from django.urls import reverse
from django.contrib.auth.models import User
from allauth.socialaccount import models


class TestUpload(TestCase):

    @classmethod
    def setUpTestData(cls):
        test_user1 = User.objects.create_user(username='testuser1', password='timpcurse123', is_superuser=True,
                                              is_staff=True)
        test_user1.save()
        models.SocialAccount.objects.create(provider='yandex', uid='1309824678', user_id=1, extra_data='')
        models.SocialApp.objects.create(provider='yandex', name='Coursework_Yandex',
                                        client_id='04497cbc7580450589324888ae5ae052', key='',
                                        secret='85b76d7cd8b04608a46c041337927ce3')
        models.SocialToken.objects.create(token='AgAAAABOElamAAbO1gZu5Ey1W0m8pNDWLBn-l9U',
                                          token_secret='1:BV_KERPV8t_XQys7:LGunSxs_asc2VKQz8pDkFRVJiAx5NX77eB6_wBvnS5Ls2bN06j30:h0yODQp8FPq3MhmKnPR8tA',
                                          account_id=1, app_id=1)

    def test_y_map(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_maps'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'yandex.maps/maps.html')

    def test_web_screen(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_web_screenshot'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'web_screenshot/web_screenshot.html')

    def test_yandex_drive(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_drive'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'upload/yandex_drive.html')

    def test_yandex_drive_upload(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_drive_upload'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('accounts_yandex_drive'))

    def test_yandex_drive_download(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_drive_download'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('accounts_yandex_drive'))


    def test_yandex_drive_mkdir(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_drive_mkdir'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('accounts_yandex_drive'))

    def test_yandex_drive_remove(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_drive_download'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('accounts_yandex_drive'))

    def test_yandex_drive_remove_trash(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_yandex_drive_remove_trash'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('accounts_yandex_drive'))

    def test_diary(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_diary'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'diary/diary.html')

    def test_calendar(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_calendar'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'calendar/calendar.html')

    def test_notes(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_notes'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'notes/notes.html')

    def test_notes_delete(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_notes_delete'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('accounts_notes'))

    def test_Upload_to_server(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('superuser_upload'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'upload/upload_to_server.html')

    def test_Upload_to_server_upload(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('superuser_upload_start'))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse('superuser_upload'))