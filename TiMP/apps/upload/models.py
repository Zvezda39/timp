from django.db import models
from django.contrib.auth.models import User


def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.id, filename)


class UploadModelManager(models.Manager):
    def create_upload(self, user, file):
        Upload = self.create(file=file, user=user)
        return Upload


class UploadModel(models.Model):
    file = models.FileField(upload_to=user_directory_path)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    objects = UploadModelManager()

class DiaryModel(models.Model):
    diary = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class NotesModel(models.Model):
    notes = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)