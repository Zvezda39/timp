from django.urls import path

from . import views

urlpatterns = [
    path('Upload_to_server/', views.Upload_View.as_view(), name='superuser_upload'),
    path('Upload_to_server/upload/', views.file_upload_view, name='superuser_upload_start'),
    path('yandex_drive/', views.YandexView.as_view(), name='accounts_yandex_drive'),
    path('yandex_drive/upload/', views.yandex_upload_view, name='accounts_yandex_drive_upload'),
    path('yandex_drive/download/', views.yandex_donwload_view, name='accounts_yandex_drive_download'),
    path('yandex_drive/mkdir/', views.yandex_mkdir_view, name='accounts_yandex_drive_mkdir'),
    path('yandex_drive/remove/', views.yandex_remove_view, name='accounts_yandex_drive_remove'),
    path('yandex_drive/remove_trash/', views.yandex_remove_trash_view, name='accounts_yandex_drive_remove_trash'),
    path('yandex_maps/', views.yandex_maps.as_view(), name='accounts_yandex_maps'),
    path('web_screenshot/', views.web_screenshot.as_view(), name='accounts_web_screenshot'),
    path('diary/', views.DiaryView.as_view(), name='accounts_diary'),
    path('calendar/', views.CalendarView.as_view(), name='accounts_calendar'),
    path('notes/', views.NotesView.as_view(), name='accounts_notes'),
    path('notes/delete/', views.Notes_delete_View, name='accounts_notes_delete'),
    ]

