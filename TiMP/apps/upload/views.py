from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from django.urls import reverse

from TiMP.apps.upload import models
from TiMP.apps.upload import forms

from allauth.socialaccount.models import SocialToken, SocialAccount

import yadisk

from django.contrib import messages
from allauth.account.adapter import get_adapter

import datetime
import time
import os
from django.conf import settings

from selenium import webdriver
from django.templatetags.static import static


class yandex_maps(TemplateView, LoginRequiredMixin):
    template_name = 'yandex.maps/maps.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(yandex_maps, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))


class web_screenshot(FormView, LoginRequiredMixin):
    template_name = 'web_screenshot/web_screenshot.html'
    form_class = forms.web_screenshot

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            options = webdriver.ChromeOptions()
            options.add_argument('--headless')
            options.add_argument("window-size=2000,5000")
            url = form.cleaned_data['url']
            DRIVER = os.path.abspath(settings.PROJECT_ROOT + static('chromedriver_win32/chromedriver.exe'))
            browser = webdriver.Chrome(DRIVER, chrome_options=options)
            browser.get(url)
            screenshot = browser.find_element_by_tag_name('body')
            context = {
                'screenshot': screenshot.screenshot_as_base64,
                'form': form,
            }
            browser.quit()
            return render(request, 'web_screenshot/web_screenshot.html', context)
        else:
            return self.form_invalid(form)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(web_screenshot, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))


class Upload_View(ListView, LoginRequiredMixin):
    template_name = 'upload/upload_to_server.html'
    context_object_name = 'files'

    def get_queryset(self):
        return models.UploadModel.objects.filter(user_id=self.request.user.id)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.is_superuser == True:
            return super(Upload_View, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))


def file_upload_view(request):
    if request.method == 'POST':
        files = request.FILES.getlist('file')
        for f in files:
            models.UploadModel.objects.create_upload(request.user, f)
        return JsonResponse({'url': reverse('superuser_upload')})
    else:
        return redirect(reverse('superuser_upload'))


def yandex_upload_view(request):
    if request.method == 'POST':
        Token = SocialToken.objects.get(
            account_id=SocialAccount.objects.filter(user_id=request.user.id, provider="yandex")[0].id)
        files = request.FILES.getlist('file')
        y = yadisk.YaDisk(token=Token.token)
        form = forms.Yandex_upload(request.POST)
        if form.is_valid():
            path = form.cleaned_data['path']
            if path == "":
                path += "/"
            elif path[-1] != '/':
                path += "/"
            overwrite = form.cleaned_data['overwrite_choice']
            if overwrite == 'yes':
                overwrite = True
            else:
                overwrite = False
            if y.is_dir(path) == True:
                for f in files:
                    try:
                        path_file = path + f.name
                        y.upload(f, path_file, overwrite=overwrite)
                    except Exception:
                        get_adapter().add_message(
                            request,
                            messages.INFO,
                            "upload/messages/" "file_already_exists.txt",
                            {"file": f, "date": datetime.datetime.now().strftime('%d %B %Y %X')},
                        )
                return JsonResponse({'url': reverse('accounts_yandex_drive')})
            else:
                get_adapter().add_message(
                    request,
                    messages.INFO,
                    "upload/messages/" "unformat_pathload.txt",
                    {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
                )
                return JsonResponse({'url': reverse('accounts_yandex_drive')})
    else:
        return redirect(reverse("accounts_yandex_drive"))


def yandex_donwload_view(request):
    if request.method == 'POST':
        Token = SocialToken.objects.get(
            account_id=SocialAccount.objects.filter(user_id=request.user.id, provider="yandex")[0].id)
        y = yadisk.YaDisk(token=Token.token)
        form = forms.Yandex_download(request.POST)
        if form.is_valid():
            path_file = form.cleaned_data['path_file']
            if y.is_file(path_file) == True:
                link = y.get_download_link(path_file)
                return redirect(link)
            else:
                get_adapter().add_message(
                    request,
                    messages.ERROR,
                    "upload/messages/" "unformat_pathdownload.txt",
                    {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
                )
                return redirect(reverse("accounts_yandex_drive"))
        else:
            get_adapter().add_message(
                request,
                messages.ERROR,
                "upload/messages/" "empty_fields.txt",
                {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
            )
            return redirect(reverse("accounts_yandex_drive"))
    else:
        return redirect(reverse("accounts_yandex_drive"))


def yandex_list_view(disk, path):
    paths = [path]
    for path_temp in list(disk.listdir(path)):
        if path_temp["type"] == "dir":
            paths += yandex_list_view(disk, path_temp["path"][5:])
        else:
            paths.append(path_temp["path"][5:])
    return paths


def yandex_mkdir_view(request):
    if request.method == 'POST':
        Token = SocialToken.objects.get(
            account_id=SocialAccount.objects.filter(user_id=request.user.id, provider="yandex")[0].id)
        y = yadisk.YaDisk(token=Token.token)
        form = forms.Yandex_mkdir(request.POST)
        if form.is_valid():
            path_dir = form.cleaned_data['path_dir']
            try:
                y.mkdir(path_dir)
                return redirect(reverse("accounts_yandex_drive"))
            except:
                get_adapter().add_message(
                    request,
                    messages.ERROR,
                    "upload/messages/" "unformat_dirpath.txt",
                    {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
                )
                return redirect(reverse("accounts_yandex_drive"))
        else:
            get_adapter().add_message(
                request,
                messages.ERROR,
                "upload/messages/" "empty_fields.txt",
                {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
            )
            return redirect(reverse("accounts_yandex_drive"))
    else:
        return redirect(reverse("accounts_yandex_drive"))


def yandex_remove_view(request):
    if request.method == 'POST':
        Token = SocialToken.objects.get(
            account_id=SocialAccount.objects.filter(user_id=request.user.id, provider="yandex")[0].id)
        y = yadisk.YaDisk(token=Token.token)
        form = forms.Yandex_remove(request.POST)
        if form.is_valid():
            path_remove = form.cleaned_data['path_remove']
            try:
                operation = y.remove(path_remove)
                if not operation is None:
                    while True:
                        status = y.get_operation_status(operation.href)
                        if status == "in-progress":
                            time.sleep(1)
                        elif status == "success":
                            break
                return redirect(reverse("accounts_yandex_drive"))
            except:
                get_adapter().add_message(
                    request,
                    messages.ERROR,
                    "upload/messages/" "unformat_deletepath.txt",
                    {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
                )
                return redirect(reverse("accounts_yandex_drive"))
        else:
            get_adapter().add_message(
                request,
                messages.ERROR,
                "upload/messages/" "empty_fields.txt",
                {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
            )
            return redirect(reverse("accounts_yandex_drive"))
    else:
        return redirect(reverse("accounts_yandex_drive"))


def yandex_remove_trash_view(request):
    if request.method == 'POST':
        Token = SocialToken.objects.get(
            account_id=SocialAccount.objects.filter(user_id=request.user.id, provider="yandex")[0].id)
        y = yadisk.YaDisk(token=Token.token)
        try:
            operation = y.remove_trash("/")
            if not operation is None:
                while True:
                    status = y.get_operation_status(operation.href)
                    if status == "in-progress":
                        time.sleep(1)
                    elif status == "success":
                        break
                get_adapter().add_message(
                    request,
                    messages.SUCCESS,
                    "upload/messages/" "remove_trash_success.txt",
                    {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
                )
            else:
                get_adapter().add_message(
                    request,
                    messages.SUCCESS,
                    "upload/messages/" "remove_trash_already.txt",
                    {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
                )
            return redirect(reverse("accounts_yandex_drive"))
        except:
            get_adapter().add_message(
                request,
                messages.ERROR,
                "upload/messages/" "remove_trash_error.txt",
                {"date": datetime.datetime.now().strftime('%d %B %Y %X')},
            )
            return redirect(reverse("accounts_yandex_drive"))
    else:
        return redirect(reverse("accounts_yandex_drive"))


class YandexView(TemplateView, LoginRequiredMixin):
    template_name = 'upload/yandex_drive.html'

    def get_context_data(self, **kwargs):
        context = super(YandexView, self).get_context_data(**kwargs)
        context['form'] = forms.Yandex_upload
        context['form_download'] = forms.Yandex_download
        context['form_mkdir'] = forms.Yandex_mkdir
        context['form_remove'] = forms.Yandex_remove
        Token = SocialToken.objects.get(
            account_id=SocialAccount.objects.filter(user_id=self.request.user.id, provider="yandex")[0].id)
        y = yadisk.YaDisk(token=Token.token)
        paths = yandex_list_view(y, "/")
        context['paths'] = paths
        return context

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if SocialAccount.objects.filter(user_id=self.request.user.id, provider="yandex").exists():
                return super(YandexView, self).dispatch(request, *args, **kwargs)
            else:
                return render(request, 'upload/yandex_drive_not_connected.html')
        return HttpResponseRedirect(reverse("index"))


class DiaryView(FormView, LoginRequiredMixin):
    template_name = 'diary/diary.html'
    form_class = forms.diary

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            diary_text = form.cleaned_data['diary']
            if models.DiaryModel.objects.filter(user_id=self.request.user.id).exists():
                models.DiaryModel.objects.filter(user_id=self.request.user.id).update(diary=diary_text)
            else:
                document = form.save(commit=False)
                document.user = self.request.user
                document.save()
            context = {'form': form}
            return render(request, 'diary/diary.html', context=context)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(DiaryView, self).get_context_data(**kwargs)
        if models.DiaryModel.objects.filter(user_id=self.request.user.id).exists():
            context['form'] = self.form_class(instance=models.DiaryModel.objects.get(user_id=self.request.user.id))
        else:
            context['form'] = self.form_class
        return context

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(DiaryView, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))


class NotesView(FormView, LoginRequiredMixin):
    template_name = 'notes/notes.html'
    form_class = forms.notes

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            document = form.save(commit=False)
            document.user = self.request.user
            document.save()
            context = {
                'form': self.form_class,
                'Notes': models.NotesModel.objects.filter(user_id=self.request.user.id),
                'form_note_delete': forms.notes_delete(user_id=self.request.user.id)
            }
            return render(request, 'notes/notes.html', context=context)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(NotesView, self).get_context_data(**kwargs)
        if models.NotesModel.objects.filter(user_id=self.request.user.id).exists():
            context['Notes'] = models.NotesModel.objects.filter(user_id=self.request.user.id)
            context['form'] = self.form_class
            context['form_note_delete'] = forms.notes_delete(user_id=self.request.user.id)
        else:
            context['form'] = self.form_class
        return context

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(NotesView, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))

def Notes_delete_View(request):
    if request.method == 'POST':
        form = forms.notes_delete(request.POST, user_id=request.user.id)
        if form.is_valid():
            note = form.cleaned_data['notes']
            models.NotesModel.objects.get(user_id=request.user.id, id=note).delete()
            return redirect(reverse("accounts_notes"))
        else:
            return request.form_invalid(form)
    else:
        return redirect(reverse("accounts_notes"))

class CalendarView(FormView, LoginRequiredMixin):
    template_name = 'calendar/calendar.html'
    form_class = forms.calendar

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(CalendarView, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))
