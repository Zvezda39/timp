from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from allauth.socialaccount.models import SocialAccount


def index(request):
    return render(request, 'account/base.html')


class ProfileList(ListView, LoginRequiredMixin):
    template_name = 'account/profile.html'
    context_object_name = 'Profile_user'
    redirect_field_name = '/'

    def get_queryset(self):
        return SocialAccount.objects.filter(user_id=self.request.user.id)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(ProfileList, self).dispatch(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("index"))
