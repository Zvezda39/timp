from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'makemessages с игнорированием ненужных подкаталогов'

    def handle(self, *args, **kwargs):
        call_command('makemessages', '-l', 'ru', '-i', '*reda_*i**')
