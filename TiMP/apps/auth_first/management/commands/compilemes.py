from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'compilemessages с игнорированием ненужных подкаталогов'

    def handle(self, *args, **kwargs):
        call_command('compilemessages', '-l', 'ru', '-i', '*reda_*i**')
