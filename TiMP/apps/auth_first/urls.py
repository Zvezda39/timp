from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('accounts/profile/', views.ProfileList.as_view(), name='accounts_profile'),
]
