from django.test import TestCase

# Create your tests here.

from django.urls import reverse
from django.contrib.auth.models import User
from allauth.socialaccount import models


class TestUpload(TestCase):

    @classmethod
    def setUpTestData(cls):
        test_user1 = User.objects.create_user(username='testuser1', password='timpcurse123', is_superuser=True,
                                              is_staff=True)
        test_user1.save()
        models.SocialAccount.objects.create(provider='yandex', uid='1309824678', user_id=1, extra_data='')
        models.SocialApp.objects.create(provider='yandex', name='Coursework_Yandex',
                                        client_id='04497cbc7580450589324888ae5ae052', key='',
                                        secret='85b76d7cd8b04608a46c041337927ce3')
        models.SocialToken.objects.create(token='AgAAAABOElamAAbO1gZu5Ey1W0m8pNDWLBn-l9U',
                                          token_secret='1:BV_KERPV8t_XQys7:LGunSxs_asc2VKQz8pDkFRVJiAx5NX77eB6_wBvnS5Ls2bN06j30:h0yODQp8FPq3MhmKnPR8tA',
                                          account_id=1, app_id=1)

    def test_profile(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('accounts_profile'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'account/profile.html')

    def test_index(self):
        login = self.client.login(username='testuser1', password='timpcurse123')
        resp = self.client.get(reverse('index'))
        self.assertEqual(str(resp.context['user']), 'testuser1')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'account/base.html')
