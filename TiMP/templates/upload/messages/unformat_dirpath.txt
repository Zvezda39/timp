{% load i18n %}
{% blocktrans %}The path to create the folder does not exist, is in the wrong format, or is located in a folder that has not yet been created. {{date}}{% endblocktrans %}